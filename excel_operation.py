import os
import openpyxl
from datetime import datetime
from openpyxl import Workbook
from openpyxl.formatting import Rule
from openpyxl.styles import Font, Color, PatternFill, Border, Alignment, GradientFill, Side, colors
from openpyxl.styles.differential import DifferentialStyle
from openpyxl.formatting.rule import ColorScaleRule, CellIsRule, FormulaRule

test_result_loc = 'TestResult/Test_Result.xlsx'


def excel_creator():
    if os.path.exists(test_result_loc):
        workbook = openpyxl.load_workbook(test_result_loc)
        worksheet1 = workbook['Summary']
        worksheet2 = workbook['Details']
        if 'Sheet' in workbook.sheetnames:
            workbook.remove(workbook['Sheet'])
    else:
        workbook = openpyxl.Workbook()
        worksheet1 = workbook.create_sheet('Summary')
        worksheet2 = workbook.create_sheet('Details')
        if 'Sheet' in workbook.sheetnames:
            workbook.remove(workbook['Sheet'])
    return workbook, worksheet1, worksheet2


def write_header():
    workbook, worksheet1, worksheet2 = excel_creator()
    worksheet2.cell(row=1, column=1).value = "S.NO"
    worksheet2.cell(row=1, column=2).value = "Test Summary"
    worksheet2.cell(row=1, column=3).value = "Result"
    worksheet2.cell(row=1, column=4).value = "Remarks"
    workbook.save(test_result_loc)


def write_summary(site, started):
    workbook, worksheet1, worksheet2 = excel_creator()
    worksheet1.cell(row=1, column=1).value = "Test Started On:"
    worksheet1.cell(row=1, column=2).value = started
    worksheet1.cell(row=2, column=1).value = "Test Executed On:"
    worksheet1.cell(row=2, column=2).value = datetime.now()
    worksheet1.cell(row=3, column=1).value = "Url"
    worksheet1.cell(row=3, column=2).value = str(site)
    worksheet1.cell(row=4, column=1).value = "Total Number of Test"
    worksheet1.cell(row=4, column=2).value = "=COUNT(Details!A:A)"
    worksheet1.cell(row=5, column=1).value = "Number of Passed Test Case"
    worksheet1.cell(row=5, column=2).value = '=COUNTIF(Details!C:C,"PASS")'
    worksheet1.cell(row=6, column=1).value = "Number of Failed Test Case"
    worksheet1.cell(row=6, column=2).value = '=COUNTIF(Details!C:C,"FAIL")'
    worksheet1.cell(row=7, column=1).value = "Number of Skipped Tested Case"
    worksheet1.cell(row=7, column=2).value = '=COUNTIF(Details!C:C,"Not Tested")'
    worksheet1.cell(row=3, column=2).alignment = Alignment(horizontal="right", vertical="center")
    blueFill = PatternFill(start_color='680000', end_color='68A0F9', fill_type='solid')
    worksheet1.conditional_formatting.add('A1:A7',
                                          FormulaRule(formula=['ISBLANK(L1)'], stopIfTrue=True, fill=blueFill))
    fit_column(worksheet1)
    workbook.save(test_result_loc)


def write_excel(sn, test_summary, result, remarks):
    workbook, worksheet1, worksheet2 = excel_creator()
    fieldnames = (int(sn), test_summary, result, str(remarks))
    start_column = 1
    start_row = sn + 1
    for fieldnames in fieldnames:
        worksheet2.cell(row=start_row, column=start_column).value = fieldnames
        start_column += 1
    format_excel(worksheet2, start_row)
    fit_column(worksheet2)
    workbook.save(test_result_loc)


def remove_file():
    if os.path.exists(test_result_loc):
        try:
            os.remove(test_result_loc)
        except OSError:
            print('File is still open')
        print("File Exits")
    else:
        print("File Doesn't Exists")


def format_excel(worksheet, start_row):
    redFill = PatternFill(start_color='EE1111', end_color='EE1111', fill_type='solid')
    greenFill = PatternFill(start_color='00AA00', end_color='00AA00', fill_type='solid')
    blueFill = PatternFill(start_color='68A0F9', end_color='68A0F9', fill_type='solid')
    orangeFill = PatternFill(start_color='FFA500', end_color='FFA500', fill_type='solid')
    character = ('A', 'B', 'C', 'D')
    for ranges in character:
        cell = ranges + str(start_row)
        worksheet.conditional_formatting.add('A1:D1',
                                             FormulaRule(formula=['ISBLANK(L1)'], stopIfTrue=True, fill=blueFill))
        worksheet.conditional_formatting.add(cell, FormulaRule(formula=['=ISNUMBER(SEARCH("FAIL",' + cell + '))'],
                                                               stopIfTrue=True, fill=redFill))
        worksheet.conditional_formatting.add(cell, FormulaRule(formula=['=ISNUMBER(SEARCH("PASS",' + cell + '))'],
                                                               stopIfTrue=True, fill=greenFill))
        worksheet.conditional_formatting.add(cell, FormulaRule(formula=['=ISNUMBER(SEARCH("Not Tested",' + cell + '))'],
                                                               stopIfTrue=True, fill=orangeFill))


def fit_column(worksheet):
    for col in worksheet.columns:
        max_length = 0
        column = col[0].column_letter
        for cell in col:
            try:
                if len(str(cell.value)) > max_length:
                    max_length = len(cell.value)
            except Exception as e:
                print(e)
        adjusted_width = (max_length + 1)
        worksheet.column_dimensions[column].width = adjusted_width
