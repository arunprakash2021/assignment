from selenium import webdriver
import time

"""def switch_window(value, index):
    try:
        driver.execute_script("window.open('');")
        driver.switch_to.window(driver.window_handles[int(index)])
        driver.get(value)
        result, remarks = result_pass()
    except Exception as e:
        result, remarks = result_fail(e)
    return result, remarks"""

driver = webdriver.Chrome(r'C:\Users\Arun\PycharmProjects\AssignmentProject\chromedriver.exe')
driver.get("https://accounts.google.com/signup/v2/webcreateaccount?hl=en-GB&flowName=GlifWebSignIn&flowEntry=SignUp")

driver.find_element_by_link_text("Help").click()

# prints parent window title
print("Parent window title: " + driver.title)

# get current window handle
p = driver.current_window_handle

# get first child window
chwd = driver.window_handles

for w in chwd:
    # switch focus to child window
    if w != p:
        driver.switch_to.window(w)
    break
time.sleep(0.9)
print("Child window title: " + driver.title)
driver.quit()
