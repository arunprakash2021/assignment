# import webdriver
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException

# import Alert 
from selenium.webdriver.common.alert import Alert

# create webdriver object
driver = webdriver.Chrome(r'C:\Users\Arun\PycharmProjects\AssignmentProject\chromedriver.exe')

# get ide.geeksforgeeks.org
# driver.get("https://ide.geeksforgeeks.org/tryit.php/WXYeMD9tD4")
driver.get(
    "https://careerkarma.com/blog/python-assert/#:~:text=The%20Python%20assert%20keyword%20tests,stop%20with%20an%20optional%20message.&text=That's%20where%20the%20Python%20assert,Python%20debugging%20to%20handle%20errors.")
try:
    WebDriverWait(driver, 10).until(EC.alert_is_present(),
                                    'Timed out waiting for PA creation ' + 'confirmation popup to appear.')

    alert = driver.switch_to.alert()
    alert.text()
    print("alert accepted")
except:
    print("no alert")
# create alert object
# alert = Alert(driver)


# accept the alert
# alert.accept()
