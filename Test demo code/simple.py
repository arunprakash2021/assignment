import time
import platform
import pandas as pd
from selenium import webdriver
from selenium.webdriver.support.ui import Select

path = r"C:\Users\Arun\PycharmProjects\AssignmentProject\chromedriver.exe"
platform = platform.platform()

if __name__ == '__main__':
    driver = webdriver.Chrome(path)
    driver.maximize_window()
    driver.get('https://www.google.com')
    driver.execute_script("window.open('');")
    driver.switch_to.window(driver.window_handles[1])
    driver.get('https://www.amazon.com')
    driver.close()
    print(platform)
    time.sleep(5)

    driver.switch_to.alert.dismiss()
    driver.quit()
