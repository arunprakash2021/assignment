import time
import pandas as pd
import excel_operation
import email_sender as es
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from num2words import num2words  # install num2words == pip install num2words

test_case_loc = 'TestCase/Automation_test_case.xlsx'


def result_pass():
    result = "PASS"
    remarks = ""
    return result, remarks


def result_fail(er):
    result = "FAIL"
    remarks = er
    return result, remarks


def read_excel():
    reader = pd.read_excel(test_case_loc)
    site_name = ''
    for row, column in reader.iterrows():
        sn = column['SN']
        flag = column['execute_flag']
        test_summary = column['Test_summary']
        xpath = column['xpath']
        action = column['action']
        value = column['value']
        if flag == "Yes":
            action_def(sn, test_summary, xpath, action, value)
        elif flag == "No":
            excel_operation.write_excel(sn, test_summary, "Not Tested", "Skipped entire row due to No Flag")
        if action == 'open_url':
            site_name = value
    return site_name


def action_def(sn, test_summary, xpath, action, value):
    # result = ''
    # remarks = ''
    try:
        if action == 'open_browser':
            result, remarks = open_browser(value)
        elif action == 'open_url':
            result, remarks = open_url(value, sn)
        elif action == 'click':
            result, remarks = click(xpath)
        elif action == 'send_value':
            result, remarks = send_value(xpath, value)
        elif action == 'select_dropdown':
            result, remarks = select_dropdown(xpath, value)
        elif action == 'wait':
            result, remarks = wait(value)
        elif action == 'compare':
            result, remarks = verify_text(xpath, value)
        elif action == 'exit':
            result, remarks = exit_window()
        elif action == 'close':
            result, remarks = close_window(value)
        elif action == 'switch':
            result, remarks = switch_window(xpath, value)
        elif action == "option_click":
            result, remarks = locating_click(xpath, value)
        else:
            result, remarks = result_fail("Action not supported by framework")
            print("Action not supported by framework")

    except Exception as e:
        print(e, "Exception has occurred")
        result = "Fail"
        remarks = e
    print(sn, test_summary, result, remarks)
    excel_operation.write_excel(sn, test_summary, result, remarks)


def open_browser(value):
    global driver
    try:
        if value == 'Chrome':
            driver = webdriver.Chrome(r'C:\Users\Arun\PycharmProjects\AssignmentProject\chromedriver.exe')
            driver.maximize_window()
            result, remarks = result_pass()
        elif value == 'firefox':
            driver = webdriver.Firefox()
            result, remarks = result_pass()
        else:
            print("Browser not supported")
            result = "FAIL"
            remarks = 'Browser Not Supported'
    except Exception as e:
        result, remarks = result_fail(e)
    return result, remarks


def open_url(value, sn):
    try:
        driver.get(value)
        result, remarks = result_pass()
        take_page_screenshot(driver, sn)
    except Exception as e:
        result, remarks = result_fail(e)
    return result, remarks


def click(xpath):
    try:
        driver.find_element_by_xpath(xpath).click()
        result, remarks = result_pass()
    except Exception as e:
        result, remarks = result_fail(e)
    return result, remarks


def send_value(xpath, value):
    try:
        driver.find_element_by_xpath(xpath).send_keys(value)
        result, remarks = result_pass()
    except Exception as e:
        result, remarks = result_fail(e)
    return result, remarks


def select_dropdown(xpath, value):
    try:
        val = driver.find_element_by_xpath(xpath)
        Select(val).select_by_visible_text(value)
        result, remarks = result_pass()
    except Exception as e:
        result, remarks = result_fail(e)
    return result, remarks


def wait(value):
    try:
        time.sleep(value)
        result, remarks = result_pass()
    except Exception as e:
        result, remarks = result_fail(e)
    return result, remarks


def verify_text(xpath, value):
    output_text = driver.find_element_by_xpath(xpath).text
    if xpath == "//title":
        output_text = driver.title
    try:
        assert value in output_text
        result, remarks = result_pass()
        # take_page_screenshot(driver, 2)
    except AssertionError:
        remarks = "Actual value is " + output_text + " Input value is " + value + " Not Matched"
        result, remarks = result_fail(remarks)
        # take_page_screenshot(driver, 3)
    return result, remarks


def close_window(index):
    try:
        driver.close()
        driver.switch_to.window(driver.window_handles[int(index)])
        result, remarks = result_pass()
    except Exception as e:
        result, remarks = result_fail(e)
    return result, remarks


def exit_window():
    try:
        driver.quit()
        result, remarks = result_pass()
    except Exception as e:
        result, remarks = result_fail(e)
    return result, remarks


def switch_window(xpath, value):
    try:
        apply_now_xpath = driver.find_element_by_xpath(xpath)
        actions = ActionChains(driver)
        # Pressing and releasing command keys
        actions.key_down(Keys.CONTROL).click(apply_now_xpath).key_up(Keys.CONTROL).perform()
        driver.switch_to.window(driver.window_handles[int(value)])
        result, remarks = result_pass()
    except Exception as e:
        result, remarks = result_fail(e)
    return result, remarks


def locating_click(xpath, value):  # Hover and click the dropdown child
    try:
        pat = driver.find_element_by_xpath(value)
        act = ActionChains(driver)
        act.move_to_element(pat).click().perform()
        act = ActionChains(driver)
        act.click(driver.find_element_by_xpath(xpath)).perform()
        result, remarks = result_pass()
    except Exception as e:
        result, remarks = result_fail(e)
    return result, remarks


def take_page_screenshot(driver, name):
    # driver.get_screenshot_as_file("TestResult/" + str(name) + "_page.png")
    driver.get_screenshot_as_file("TestResult/" + num2words(name) + "_page_screenshot.png")


if __name__ == "__main__":
    try:
        started_time = datetime.now()
        excel_operation.remove_file()
        excel_operation.write_header()
        site = read_excel()
        excel_operation.write_summary(site, started_time)
        time.sleep(5)
        # es.send_selenium_report()

    except Exception as e:
        print(e)
