# https://myaccount.google.com/u/0/lesssecureapps?pli=1 >> To Allow
import os
import smtplib
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email import encoders
from idlelib.multicall import r


def send_selenium_report():
    try:
        msg = MIMEMultipart()
        # Config Reading Part
        filename = 'email.config'
        utf_str = open(filename).read()
        config = eval(utf_str)
        from_add = config['email_sender']
        to_add = config['email_receiver']
        password = config['email_login_password']
        # Email Sending Part
        msg['From'] = from_add
        msg['To'] = to_add
        msg['Subject'] = "Assignment Submission"
        body = MIMEText("Dear Sir, <br> <br> Assignment has been submitted with Test case, Test result attached "
                        "to mail and code is attached to bitbucket as link is attached in the mail:<br><br>"
                        " Code Link : https://bitbucket.org/arunprakash2021/assignment/src/master/ <br><br>"
                        "<b>Note: This Mail was send through Automation testing as a part of assignment.</b>"
                        "<br><br>And Please inform us how can i get certificate of the course.<br>"
                        "<br><br>It was a great experience to learn from you QA training "
                        "<br><br> Thank You,<br> Arun Prakash", 'html', 'utf-8')
        msg.attach(body)
        file_path = 'TestResult/'
        for subdir, dirs, files in os.walk(file_path):
            for filename in files:
                attachment = open(file_path + filename, "rb")
                part = MIMEBase('application', 'octet-stream')
                part.set_payload(attachment.read())
                encoders.encode_base64(part)
                part.add_header('Content-Disposition', "attachment; filename= %s" % filename)
                msg.attach(part)
        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.starttls()
        server.login(from_add, password)
        text = msg.as_string()
        server.sendmail(from_add, to_add.split(','), text)
        print("Email sent Successfully")
        server.quit()
    except Exception as e:
        print(e)
